Epidemiologist, Scientists, Statisticians, Historians, Data engineers and Data scientists are working on finding descriptive models and theories to explain COVID-19 expansion phenomena or on building  analytics predictive models for learning the apex of COVID-19 confimed cases, recovered cases, and deaths evolution curves.

In CRISP-DM life cycle, 75% of time is consumed only by data preparation phase causing lot of pressions and stress on scientists and data scientists building machine learning models.

This gitlab repository aims to help reducing data preparation efforts of COVID-19 Data :

- HBase-and-Hive directory : presents detailed schemas design and data preparation technical scripts for formatting and storing Johns Hopkins University COVID-19 daily data in HBase NoSQL data store, and enabling HiveQL COVID-19 data querying in a relational Hive SQL-like style.

- Linux-Shell-Scripting directory : presents detailed detailed data preparation shell scripts for
formatting and normalising Johns Hopkins University COVID-19 daily data via three user stories applying data preparation at lexical, syntactic & semantics and pragmatic levels targetting data engineers and data sci-
entists aiming to deliver in an agile analytics lifecycle adapted to critical COVID-19 context.


other scripts are to come...


This work is to be taken as a leveraging bootstrap for specific data preparation phase in COVID-19 analytics Big Data projects aiming for instance to study correlations between COVID-19 curves with humidity data, people telco mobilty during countries lockdown phases, or to analyse recurrent COVID-19 contamination causality, or to study similarities with other historical pandemics evolution data like SARS-CoV, MERS-COV or to integrate with medical/biology best practices, COVID-19 mutations, pandemics, scientific papers results, etc.


The more integration you do on the schema with other data sets (e.g. continents, median age, population, testing numbers, virus contamination rates, etc.), the more features you will have and the more this work will leverage your COVID-19 data experience. Hurry Up, and share you experience for the world scientists.


Acknowledgment :

Acknowledgement must go to Johns Hopkins University Center for Systems Science and Engineering (JHU CCSE) for keeping up to date world wide COVID-
19 data available in a daily frequency.

Acknowledgement must also go to The Ministry of National Education, Higher Education, Staff Training, and Scientific Research, Morocco for accepting and
supporting my sabbatical leave to do research, and return to ENSIAS refreshed. I also acknowledge my colleagues at ENSIAS maintaining the superb teaching
and learning and e-learning culture in the school in my absence especially during COVID-19 crisis.

